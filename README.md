
<!-- README.md is generated from README.Rmd. Please edit that file -->

# kobolite

<!-- badges: start -->
<!-- badges: end -->

## Installation

You can install the development version of kobolite from
[Framagit](https://framagit.org/) with:

``` r
# install.packages(remotes)
remotes::install_git("https://framagit.org/espace-dev/geohealth/kobolite")
library(kobolite)
```
